#!/usr/bin/env node

const args = process.argv.reduce(
    ( args, arg ) => {
        if ( arg.match(/^--/) ) {
            const [ parameter, value ] = arg.replace(/^--/,'').split('=');
            args[parameter] = value;
        }
            
        return args;
    },
    {}
);

if( ! args.day  || ! args.data ) {
    console.error( "missing requried parameters\n--day=<DAY> --data=<DATA_FILE>\n" );
    process.exit(1);
}

const day = require( `../lib/Day-${args.day.toString().padStart(2, 0)}.js` );
const data = loadData( args.data );

const answer1 = day.part1( data );
console.log( `part 1 - ${answer1}` );

const answer2 = day.part2( data );
console.log( `part 2 - ${answer2}` );

process.exit();


function loadData( file ) {
    const fs = require('fs');
    return JSON.parse( fs.readFileSync( file ) );
}
