const { sum } = require( '../lib/Utils' );

exports.part1 = function ( trees ) {
    let visible = 0;

    trees.forEach( ( row, r ) => {
        row.forEach( (tree, c ) => {
            visible += Math.max(
                isVisibleFromNorth( trees, r, c ),
                isVisibleFromEast( trees, r, c ),
                isVisibleFromSouth( trees, r, c ),
                isVisibleFromWest( trees, r, c ),
            );
        });
    } );

    return visible;
};

exports.part2 = function ( trees ) {
    let bestScore = 0;

    trees.forEach( ( row, r ) => {
        row.forEach( (tree, c ) => {
            bestScore = Math.max(
                bestScore,

                viewNorth( trees, r, c ) *
                viewEast( trees, r, c ) *
                viewSouth( trees, r, c ) *
                viewWest( trees, r, c )
            );
        });
    } );

    return bestScore;
};

function isVisibleFromNorth( trees, r, c ) {
    for( let i = r - 1;  i >= 0; i-- ) {
        if ( trees[r][c] <= trees[i][c] ) {
            return 0;
        }
    }

    return 1;
}

function isVisibleFromSouth( trees, r, c ) {
    for( let i = r + 1;  i < trees.length; i++ ) {
        if ( trees[r][c] <= trees[i][c] ) {
            return 0;
        }
    }

    return 1;
}

function isVisibleFromEast( trees, r, c ) {
    for( let i = c + 1;  i < trees[r].length; i++ ) {
        if ( trees[r][c] <= trees[r][i] ) {
            return 0;
        }
    }

    return 1;
}

function isVisibleFromWest( trees, r, c ) {
    for( let i = c - 1;  i >= 0; i-- ) {
        if ( trees[r][c] <= trees[r][i] ) {
            return 0;
        }
    }

    return 1;
}

function viewNorth( trees, r, c ) {
    let seen = 0;

    for( let i = r - 1;  i >= 0; i-- ) {
        seen++;
        if ( trees[r][c] <= trees[i][c] ) {
            return seen;
        }
    }

    return seen;
}

function viewSouth( trees, r, c ) {
    let seen = 0;

    for( let i = r + 1;  i < trees.length; i++ ) {
        seen++;

        if ( trees[r][c] <= trees[i][c] ) {
            return seen;
        }
    }

    return seen;
}

function viewEast( trees, r, c ) {
    let seen = 0;

    for( let i = c + 1;  i < trees[r].length; i++ ) {
        seen++;

        if ( trees[r][c] <= trees[r][i] ) {
            return seen;
        }
    }

    return seen;
}

function viewWest( trees, r, c ) {
    let seen = 0;

    for( let i = c - 1;  i >= 0; i-- ) {
        seen++;

        if ( trees[r][c] <= trees[r][i] ) {
            return seen;
        }
    }

    return seen;
}
