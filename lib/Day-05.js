exports.part1 = function ( { stacks, instructions } ) {
    const moves = instructions.flatMap( parseInstruction );
    const finalStacks = moves.reduce(
        ( stacks, [ from, to ] ) => {
            stacks[to].push( stacks[from].pop() );
            return stacks;
        },
        JSON.parse( JSON.stringify( stacks ) )
    );
    
    return finalStacks.map( ( stack ) => stack.pop() ).join( "" );
};

exports.part2 = function ( { stacks, instructions } ) {
    const moves = instructions.map( parseInstruction2 );
    const finalStacks = moves.reduce(
        ( stacks, [ from, to, size ] ) => {
            stacks[to].push( ...stacks[from].splice( -size, size ) );
            return stacks;
        },
        JSON.parse( JSON.stringify( stacks ) )
    );
    
    return finalStacks.map( ( stack ) => stack.pop() ).join( "" );
};

function parseInstruction( instruction ) {
    const pattern = /move (\d+) from (\d+) to (\d+)/;

    const inst = instruction.match( pattern );

    const moves = [];
    for( let i = 0; i < inst[1]; i++ ) {
        moves.push( [ inst[2] - 1, inst[3] - 1 ] );
    }

    return moves;
}

function parseInstruction2( instruction ) {
    const pattern = /move (\d+) from (\d+) to (\d+)/;

    const inst = instruction.match( pattern );

    return [ inst[2] - 1, inst[3] - 1, inst[1] ];
}
