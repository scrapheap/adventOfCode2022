const { sum } = require( '../lib/Utils' );

exports.part1 = function ( guide ) {
    return sum( guide.map( decrypt ).map( score ) );
};

exports.part2 = function ( guide ) {
    return sum( guide.map( decrypt2 ).map( score ) );
};

const values = {
    'R':  1,
    'P':  2,
    'S':  3,
    'RR': 3,
    'RP': 6,
    'RS': 0,
    'PR': 0,
    'PP': 3,
    'PS': 6,
    'SR': 6,
    'SP': 0,
    'SS': 3
};

function score( round ) {
    return values[ round[1] ] + values[ round[0] + round[1] ];
}

const key = {
    'A': 'R',
    'B': 'P',
    'C': 'S',
    'X': 'R',
    'Y': 'P',
    'Z': 'S'
};

function decrypt( round ) {
    return round.map( ( v ) => key[v] );
}

const key2 = {
    'A':  'R',
    'B':  'P',
    'C':  'S',
    'RX': 'S',
    'RY': 'R',
    'RZ': 'P',
    'PX': 'R',
    'PY': 'P',
    'PZ': 'S',
    'SX': 'P',
    'SY': 'S',
    'SZ': 'R'
};

function decrypt2( round ) {
    return [
        key2[ round[0] ],
        key2[ key2[ round[0] ] + round[1] ]
    ];
}
