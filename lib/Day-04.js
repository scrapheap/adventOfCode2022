exports.part1 = function ( assignments ) {
    return assignments.filter( covering ).length;
};

exports.part2 = function ( assignments ) {
    return assignments.filter( overlaps ).length;
};

function covering( assignment ) {
    return (
        ( assignment[0][0] <= assignment[1][0] && assignment[0][1] >= assignment[1][1] )
        || ( assignment[1][0] <= assignment[0][0] && assignment[1][1] >= assignment[0][1] )
    );
}

function overlaps( assignment ) {
    return (
        ( assignment[0][0] <= assignment[1][0]  &&  assignment[1][0] <= assignment[0][1] )
        || ( assignment[1][0] <= assignment[0][0]  &&  assignment[0][0] <= assignment[1][1] )
    );
}
