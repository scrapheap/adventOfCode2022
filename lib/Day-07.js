const { sum } = require( '../lib/Utils' );

exports.part1 = function ( session ) {
    const directories = parse( session );
    return sum( Object.values( directories ).filter( ( size ) => ( size <= 100000 ) ) );
};

exports.part2 = function ( session ) {
    const directories = parse( session );

    const sizeRequired = 30000000;
    const maxSpace = 70000000;

    const spaceNeeded = sizeRequired - ( maxSpace - directories['/'] );

    const deletable = Object.keys( directories ).reduce(
        ( best, current ) => {
            return ( directories[ current ] > spaceNeeded && directories[current] < directories[best] ) ? current : best;
        },
        '/'
    );

    return directories[ deletable ];
};

function parse( session ) {
    const directories = {};

    const path = [ '' ];

    session.forEach( ( line ) => {
        const dir = line.match( /^\$ cd (.+)/ );
        if( dir ) {
            switch( dir[1] ) {
                case '/':
                    path.length = 0;
                    break;
                case '..':
                    path.pop();
                    break;
                default:
                    path.push( dir[1] )
                    break;
            }
            
        } else {
            const fileSize = line.match( /^(\d+)/ );

            if ( fileSize ) {
                for( let i = 0; i <= path.length; i++ ) {
                    const currentPath = '/' + path.slice( 0, i ).join( '/' );
                    const currentSize = directories[ currentPath ] || 0;

                    directories[ currentPath ] = currentSize + parseInt(fileSize[1], 10);
                }
            }
        }
    } );

    return directories
}
