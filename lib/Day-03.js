const { sum, ord } = require( '../lib/Utils' );

exports.part1 = function ( rucksacks ) {
    return sum( rucksacks.map( inBoth ).map( score ) );
};

exports.part2 = function ( rucksacks ) {
    return sum( group( rucksacks ).map( inAll ).map( score ) );
};




function inBoth( rucksack ) {
    const left = rucksack.slice( 0, rucksack.length / 2 );
    const right = rucksack.slice( rucksack.length / 2, rucksack.length );

    const re = new RegExp( '([' + right + '])' );

    return left.match( re )[1];
}

function score( item ) {
    const itemCode = ord( item );

    return ( itemCode & 32 ) ?  1 + ( itemCode - ord( 'a' ) ) :  27 + ( itemCode - ord( 'A' ) );
}

function group( rucksacks ) {
    return rucksacks.reduce(
        ( groups, rucksack ) => {
            if ( groups[  groups.length - 1 ].length > 2 ) {
                groups.push( [] );
            }

            groups[  groups.length - 1 ].push( rucksack );

            return groups;
        },
        [ [] ]
    );
}

function inAll( rucksacks ) {
    const notInSecond = new RegExp( '[^' + rucksacks[1] + ']', 'g' );
    const notInThird = new RegExp( '[^' + rucksacks[2] + ']', 'g' );

    return rucksacks[0].replaceAll( notInSecond, "" ).replaceAll( notInThird, "" );
}
