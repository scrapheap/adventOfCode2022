const { nsortDesc, sum } = require( '../lib/Utils' );

exports.part1 = function ( elfSnacks ) {
    return Math.max( ...elfSnacks.map( sum ) )
};

exports.part2 = function ( elfSnacks ) {
    const totals = nsortDesc( elfSnacks.map( sum ) );

    return sum( totals.slice(0,3) );
    
};
