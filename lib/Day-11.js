const { sum } = require( '../lib/Utils' );


exports.part1 = function ( troop ) {
    const monkeys = JSON.parse( JSON.stringify( troop ) );
    monkeys.forEach( ( monkey ) => {
        monkey.inspectionCount = 0;
        monkey.op = eval( "( x ) => " + monkey.operation + ";" );
        monkey.next = eval( "( x ) => ( x % " + monkey.test[0] + " ) == 0 ? " + monkey.test[1] + " : " + monkey.test[2] + ";" );
    } );

    for( let round = 0; round < 20; round++ ) {
        monkeys.forEach( ( monkey ) => {
            while( monkey.items.length ) {
                monkey.inspectionCount++;

                const item = Math.floor( monkey.op( monkey.items.shift() ) / 3 );
                monkeys[ monkey.next( item ) ].items.push( item );
            }
        } );
    }

    const inspections = monkeys.map( ( monkey ) => monkey.inspectionCount ).sort( ( a, b ) => b - a );

    return inspections[0] * inspections[1];

};

exports.part2 = function ( troop ) {
    const monkeys = JSON.parse( JSON.stringify( troop ) );
    const worryMod = monkeys.reduce( (worryMod, monkey) => { return worryMod * monkey.test[0] }, 1 ); 

    monkeys.forEach( ( monkey ) => {
        monkey.inspectionCount = 0;
        monkey.op = eval( "( x ) => " + monkey.operation + ";" );
        monkey.next = eval( "( x ) => ( x % " + monkey.test[0] + " ) == 0 ? " + monkey.test[1] + " : " + monkey.test[2] + ";" );
    } );

    for( let round = 0; round < 10000; round++ ) {
        monkeys.forEach( ( monkey ) => {
            while( monkey.items.length ) {
                monkey.inspectionCount++;

                const item = Math.floor( monkey.op( monkey.items.shift() ) % worryMod );
                monkeys[ monkey.next( item ) ].items.push( item );
            }
        } );
    }

    const inspections = monkeys.map( ( monkey ) => monkey.inspectionCount ).sort( ( a, b ) => b - a );

    return inspections[0] * inspections[1];
};
