const { sum } = require( '../lib/Utils' );


exports.part1 = function ( program ) {
    const code = parse( program );

    let cycle = 0;
    let x = 1;

    const xVals = [];
    code.forEach( ( instruction ) => {
        cycle++;
        if ( logCycle( cycle ) ) {
            xVals.push( x * cycle );
        }

        if( instruction[0] == "addx" ) {
            x += instruction[1];
        }   
    } );

    return sum( xVals );
};

exports.part2 = function ( program ) {
    const code = parse( program );

    let cycle = 0;
    let x = 1;

    const crt = [ "\n" ];
    code.forEach( ( instruction ) => {
        crt.push( Math.abs( x - ( cycle % 40 ) ) <= 1 ? '#' : '.' );
        cycle++;
        if( cycle % 40 == 0 ) {
            crt.push( "\n" );
        }

        if( instruction[0] == "addx" ) {
            x += instruction[1];
        }   
    } );

    return crt.join('');
};

function parse( program ) {
    return program
        .map( ( line ) => line.split( /\s+/ ) )
        .flatMap( ( instruction ) => {
            return  instruction[0] == "addx" ? [ [ "noop" ], [ instruction[0], parseInt( instruction[1], 10 ) ] ] : [ instruction ];
        } );
}

function logCycle( cycle ) {
    return ! ( ( cycle +20 ) % 40 );
}
