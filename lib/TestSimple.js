exports.test = function( name, callback ) {
    let success = 1;
    console.log( name + ' ...' );
    try {
        callback()
    } catch(e) {
        console.log( e );
        success = 0;
    }
    console.log( success ? 'Result ... success' : 'Result ... failure' );
}
