// const { sum } = require( '../lib/Utils' );

exports.part1 = function ( moves ) {
    const head = { "x": 0, "y": 0 };
    const tail = { "x": 0, "y": 0 };

    const tailPositions = moves.reduce (
        (positions, move) => {
            for( let i = 0; i < move[1]; i++ ) {
                switch( move[0] ) {
                    case "R": head.y++;
                        break;
                    case "L": head.y--;
                        break;
                    case "U": head.x++;
                        break;
                    case "D": head.x--;
                        break;
                }

                if( ! nextTo( head, tail ) ) {
                    tail.x += stepTowards( head.x, tail.x );
                    tail.y += stepTowards( head.y, tail.y );
                }

                positions[ [ tail.x, tail.y ].join(',') ] = 1;
            }
            
            return positions;
        },
        { "0,0": 1}
    );

    return Object.keys( tailPositions ).length;
};

exports.part2 = function ( moves ) {
    const rope = [
        { "x": 0, "y": 0 },
        { "x": 0, "y": 0 },
        { "x": 0, "y": 0 },
        { "x": 0, "y": 0 },
        { "x": 0, "y": 0 },
        { "x": 0, "y": 0 },
        { "x": 0, "y": 0 },
        { "x": 0, "y": 0 },
        { "x": 0, "y": 0 },
        { "x": 0, "y": 0 }
    ];

    const tailPositions = moves.reduce (
        (positions, move) => {
            for( let i = 0; i < move[1]; i++ ) {
                switch( move[0] ) {
                    case "R": rope[0].y++;
                        break;
                    case "L": rope[0].y--;
                        break;
                    case "U": rope[0].x++;
                        break;
                    case "D": rope[0].x--;
                        break;
                }
                for( let knot = 1; knot < rope.length; knot++ ) {
                    if( ! nextTo( rope[ knot -1], rope[ knot ] ) ) {
                        rope[ knot ].x += stepTowards( rope[ knot - 1 ].x, rope[ knot ].x );
                        rope[ knot ].y += stepTowards( rope[ knot - 1 ].y, rope[ knot ].y );
                    }
                }

                positions[ [ rope[ rope.length - 1].x, rope[ rope.length - 1 ].y ].join(',') ] = 1;
            }
            
            return positions;
        },
        { "0,0": 1}
    );

    return Object.keys( tailPositions ).length;
};

function nextTo( a, b ) {
    return Math.abs( a.x - b.x ) <= 1 && Math.abs( a.y - b.y ) <= 1;
}

function stepTowards( a, b ) {
    if ( a > b ) {
        return 1;
    }

    if ( a < b ) {
        return -1;
    }

    return 0;
}
