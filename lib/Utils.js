exports.nsort = ( list ) => {
    return list.sort( (a, b) => a - b );
};

exports.nsortDesc = ( list ) => {
    return list.sort( (a, b) => b - a );
};

exports.sum = ( list ) => {
    return list.reduce( ( a, b ) => a + b, 0 );
};

exports.ord = ( c ) => {
    return c.charCodeAt( 0 ); 
};
